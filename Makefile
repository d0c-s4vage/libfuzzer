SRC_DIR := .
OBJ_DIR := .
SRC_FILES := $(wildcard $(SRC_DIR)/*.cpp)
OBJ_FILES := $(patsubst $(SRC_DIR)/%.cpp,$(OBJ_DIR)/%.cpp.o,$(SRC_FILES))
CXX := clang

libFuzzer.a: $(OBJ_FILES)
	ar ruv libFuzzer.a Fuzzer*.o

$(OBJ_DIR)/%.cpp.o: $(SRC_DIR)/%.cpp
	$(CXX) -g -O2 -fno-omit-frame-pointer -std=c++11 -o $@ -c $<

clean:
	rm $(OBJ_DIR)/*.o libFuzzer.a
